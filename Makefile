all:
	cargo check

debug:
	cargo build

release:
	cargo build --release

test:
	cargo test

clean:
	git clean -dxf

clippy:
	rustup run nightly cargo clippy
