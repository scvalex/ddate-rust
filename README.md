ddate-rust
==========

Tool and library to convert Gregorian dates to Discordian dates.
Drop-in replacement for the ddate tool from util-linux.

[![pipeline status](https://gitlab.com/scvalex/ddate-rust/badges/master/pipeline.svg)](https://gitlab.com/scvalex/ddate-rust/commits/master)

Tool
----

Run:

    $ cargo install
    $ ddate
    Today is Pungenday, the 21st day of The Aftermath in the YOLD 3183
    $ ddate '+%d %b %Y'
    21 Afm 3183
    $ ddate 30 01 2017
    Setting Orange, Chaos 30, 3183 YOLD
    $ ddate '+%d %b %Y' 30 01 2017
    30 Chs 3183

Library
-------

Add this to your `Cargo.toml`:

    [dependencies]
    ddate = { git = "https://github.com/scvalex/ddate-rust.git", tag = "1.0.0" }

and this to your crate root:

    extern crate ddate;
