//! Library to convert Gregorian dates to Discordian dates.
//!
//! The Discordian or Erisian calendar is an alternative calendar used
//! by some adherents of Discordianism. It is specified on page 00034
//! of the Principia Discordia.  See
//! [Wikipedia](https://en.wikipedia.org/wiki/Discordian_calendar) for
//! more details.

extern crate rand;

use std::borrow::Cow;
use std::error;
use std::fmt;

const DAY_LONG: &[&str] = &[
    "Sweetmorn",
    "Boomtime",
    "Pungenday",
    "Prickle-Prickle",
    "Setting Orange",
];

const DAY_SHORT: &[&str] = &["SM", "BT", "PD", "PP", "SO"];

const SEASON_LONG: &[&str] = &[
    "Chaos",
    "Discord",
    "Confusion",
    "Bureaucracy",
    "The Aftermath",
];

const SEASON_SHORT: &[&str] = &["Chs", "Dsc", "Cfn", "Bcy", "Afm"];

const HOLYDAY: &[&[&str]] = &[
    &["Mungday", "Chaoflux"],
    &["Mojoday", "Discoflux"],
    &["Syaday", "Confuflux"],
    &["Zaraday", "Bureflux"],
    &["Maladay", "Afflux"],
];

const EXCL: &[&str] = &[
    "Hail Eris!",
    "All Hail Discordia!",
    "Kallisti!",
    "Fnord.",
    "Or not.",
    "Wibble.",
    "Pzat!",
    "P'tang!",
    "Frink!",
    "Grudnuk demand sustenance!",
    "Keep the Lasagna flying!",
    "You are what you see.",
    "Or is it?",
    "This statement is false.",
    "Lies and slander, sire!",
    "Hee hee hee!",
    "",
];

const CAL: &[i32] = &[31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

/// Type of Discordian dates.
#[derive(Clone, Debug, Eq, PartialEq, PartialOrd, Ord)]
pub struct DDate {
    /// Year of Our Lady of Discord: 3066-
    year: i32,
    /// Season: 0-4
    season: i32,
    /// Day within season: 0-72
    day: i32,
}

/// Format that results in "Setting Orange, The Aftermath 18, 3183
/// YOLD".
pub const DEFAULT_FMT: &str = "%{%A, %B %d%}, %Y YOLD";

/// Format that results in "Today is Sweetmorn, the 19th day of The
/// Aftermath in the YOLD 3183".
pub const DEFAULT_FMT_LONG: &str = "Today is %{%A, the %e day of %B%} in the YOLD %Y%N%nCelebrate %H";

impl fmt::Display for DDate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.format("%{%B %d%}, %Y YOLD"))
    }
}

impl DDate {
    /// Convert a Gregorian year, month, day triple to a Discordian
    /// date.
    ///
    /// # Example
    ///
    /// ```
    /// use ddate::DDate;
    ///
    /// let ddate = DDate::from_gregorian(2017, 11, 06).unwrap();
    /// assert_eq!(ddate.to_string(), "The Aftermath 18, 3183 YOLD");
    pub fn from_gregorian(iyear: i32, imonth: i32, iday: i32) -> Result<DDate, Error> {
        let mut imonth = imonth;
        if imonth < 1 || imonth > 12 {
            return Err(Error(format!("invalid Gregorian month: {}", imonth)));
        }
        if iyear == 0 {
            return Err(Error(format!("invalid Gregorian year: {}", iyear)));
        }
        if (iday < 1 || iday > CAL[imonth as usize - 1]) &&
            !(imonth == 2 && iday == 29 && iyear % 4 == 0 &&
                  (iyear % 100 != 0 || iyear % 400 == 0))
        {
            return Err(Error(format!(
                "invalid day in month/year: {}-{:02}-{:02}",
                iyear,
                imonth,
                iday
            )));
        }
        imonth -= 1;
        // note: gregorian year 0 doesn't exist so add one if user specifies a year
        // less than 0
        let year = iyear + 1166 + (if 0 > iyear { 1 } else { 0 });
        let mut dayspast = 0;
        while imonth > 0 {
            imonth -= 1;
            dayspast += CAL[imonth as usize];
        }
        let mut day = dayspast + iday - 1;
        let mut season = 0;
        if year % 4 == 2 && day == 59 && iday == 29 {
            day = -1
        }
        while day >= 73 {
            season += 1;
            day -= 73;
        }
        Ok(DDate { season, day, year })
    }

    fn yday(&self) -> i32 {
        self.season * 73 + self.day
    }

    /// Convert the date to a string according to the formatting
    /// string.  Special formatting sequences are:
    ///
    /// * `%A` - Full name of the day of the week (e.g. Sweetmorn)
    /// * `%a` - Abbreviated name of the day of the week (e.g. SM)
    /// * `%B` - Full name of the season (e.g. Chaos)
    /// * `%b` - Abbreviated name of the season (e.g. Chs)
    /// * `%d` - Ordinal number of day in season (e.g. 23)
    /// * `%e` - Cardinal number of day in season (e.g. 23rd)
    /// * `%H` - Name of current Holyday, if any
    /// * `%N` - Magic code to prevent rest of format from being
    /// printed unless today is a Holyday.
    /// * `%n` - Newline
    /// * `%t` - Tab
    /// * `%X` - Number of days remaining until X-Day
    /// * `%Y` - Year (e.g. 3183)
    /// * `%{ and %}` - Used to enclose the part of the string which
    /// is to be replaced with the words "St. Tib's Day" if the
    /// current day is St. Tib's Day.
    /// * `%.` - Try it and see.
    ///
    /// # Example
    ///
    /// ```
    /// use ddate::DDate;
    ///
    /// let ddate = DDate::from_gregorian(2017, 11, 06).unwrap();
    /// assert_eq!(
    ///     ddate.format("%{%A, %B %d%}, %Y YOLD"),
    ///     "Setting Orange, The Aftermath 18, 3183 YOLD");
    /// assert_eq!(
    ///     ddate.format("%{%A, the %e day of %B%} in the YOLD %Y%N%nCelebrate %H"),
    ///     "Setting Orange, the 18th day of The Aftermath in the YOLD 3183");
    /// let ddate = DDate::from_gregorian(2017, 09, 26).unwrap();
    /// assert_eq!(
    ///     ddate.format("%{%A, the %e day of %B%} in the YOLD %Y%N%nCelebrate %H"),
    ///     "Prickle-Prickle, the 50th day of Bureaucracy in the YOLD 3183\nCelebrate Bureflux");
    /// let ddate = DDate::from_gregorian(2016, 02, 29).unwrap();
    /// assert_eq!(
    ///     ddate.format("%{%A, the %e day of %B%} in the YOLD %Y%N%nCelebrate %H"),
    ///     "St. Tib\'s Day in the YOLD 3182");
    pub fn format(&self, fmt: &str) -> String {
        let mut tib_start: i32 = -1;
        let mut tib_end: i32 = 0;
        for (i, (c1, c2)) in fmt.chars().zip(fmt.chars().skip(1)).enumerate() {
            let i = i as i32;
            if c1 == '%' {
                match c2 {
                    'A' | 'a' | 'd' | 'e' => {
                        if tib_start > 0 {
                            tib_end = i + 1;
                        } else {
                            tib_start = i;
                        }
                    }
                    '{' => tib_start = i,
                    '}' => tib_end = i + 1,
                    _ => (),
                }
            }
        }
        let fmt: Vec<char> = fmt.chars().collect();
        let mut i = 0;
        let fmt_len = fmt.len();
        let mut res = Vec::with_capacity(fmt_len);
        while i < fmt_len as i32 {
            if i == tib_start && self.day == -1 {
                res.push(Cow::from("St. Tib's Day"));
                i = tib_end;
            } else if fmt[i as usize] == '%' {
                i += 1;
                let formatted = match fmt[i as usize] {
                    'A' => Cow::from(DAY_LONG[(self.yday() % 5) as usize]),
                    'a' => Cow::from(DAY_SHORT[(self.yday() % 5) as usize]),
                    'B' => Cow::from(SEASON_LONG[self.season as usize]),
                    'b' => Cow::from(SEASON_SHORT[self.season as usize]),
                    'd' => Cow::from(format!("{}", self.day + 1)),
                    'e' => Cow::from(format!("{}{}", self.day + 1, ending(self.day + 1))),
                    'H' => {
                        if self.day == 4 || self.day == 49 {
                            let holiday_or_not = if self.day == 49 { 1 } else { 0 };
                            Cow::from(HOLYDAY[self.season as usize][holiday_or_not])
                        } else {
                            Cow::from("")
                        }
                    }
                    'N' => {
                        if self.day != 4 && self.day != 49 {
                            i = fmt.len() as i32
                        }
                        Cow::from("")
                    }
                    'n' => Cow::from("\n"),
                    't' => Cow::from("\t"),
                    'Y' => Cow::from(format!("{}", self.year)),
                    '.' => Cow::from(EXCL[rand::random::<usize>() % EXCL.len()]),
                    'X' => Cow::from(format!("{}", xday_countdown(self.yday(), self.year))),
                    _ => Cow::from(""),
                };
                res.push(formatted)
            } else {
                res.push(Cow::from(format!("{}", fmt[i as usize])))
            }
            i += 1;
        }
        res.join("")
    }
}

fn ending(i: i32) -> &'static str {
    if i / 10 == 1 {
        "th"
    } else {
        match i % 10 {
            1 => "st",
            2 => "nd",
            3 => "rd",
            _ => "th",
        }
    }
}

//  Code for counting down to X-Day, X-Day being Cfn 40, 3164
//
//  After `X-Day' passed without incident, the CoSG declared that it had
//  got the year upside down --- X-Day is actually in 8661 AD rather than
//  1998 AD.
//
//  Thus, the True X-Day is Cfn 40, 9827.

fn xday_countdown(yday: i32, year: i32) -> i32 {
    let mut year = year;
    let mut r = (185 - yday) + (if yday < 59 && leapp(year) { 1 } else { 0 });
    while year < 9827 {
        year += 1;
        r += if leapp(year) { 366 } else { 365 };
    }
    while year > 9827 {
        r -= if leapp(year) { 366 } else { 365 };
        year -= 1;
    }
    r
}

fn leapp(i: i32) -> bool {
    ((i + 1166) % 4 == 0) && (((i + 1166) % 100 != 0) || ((i + 1166) % 400 == 0))
}

/// Type of ddate errors.
#[derive(Debug)]
pub struct Error(String);

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let &Error(ref msg) = self;
        write!(f, "{}", msg)
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        let &Error(ref msg) = self;
        msg
    }
}
