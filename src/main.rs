extern crate ddate;
extern crate time;

use ddate::DDate;
use std::io::{stderr, Write};

enum Res {
    Done(String),
    Version,
    Usage(Option<String>),
}

use Res::*;

fn main() {
    let mut args = std::env::args();
    let progname = args.next().unwrap();
    let progname: &str = match progname.rsplit('/').next() {
        None => &progname,
        Some(basename) => basename,
    };
    match parse_args(&mut args) {
        Ok(Done(output)) => println!("{}", output),
        Ok(Version) => println!("{} ({})", progname, env!("CARGO_PKG_VERSION")),
        Ok(Usage(err)) => usage(progname, &err),
        Err(err) => {
            println!("{}", err);
            std::process::exit(1);
        }
    }
}

fn parse_args(args: &mut std::env::Args) -> Result<Res, ddate::Error> {
    let mut fmt = None;
    let mut remaining_args = vec![];
    for arg in args {
        let arg = arg.chars().as_str();
        match &arg[0..1] {
            "+" => fmt = Some(String::from(&arg[1..])),
            "-" => {
                match &arg[1..2] {
                    "v" => return Ok(Version),
                    "h" => return Ok(Usage(None)),
                    x => return Ok(Usage(Some(format!("unknown option: -{}", x)))),
                }
            }
            _ => remaining_args.push(String::from(arg)),
        }
    }

    let (ddate, fmt) = if remaining_args.len() == 3 {
        let iday: i32 = remaining_args[0].parse().expect("number");
        let imonth: i32 = remaining_args[1].parse().expect("number");
        let iyear: i32 = remaining_args[2].parse().expect("number");
        let ddate = DDate::from_gregorian(iyear, imonth, iday)?;
        (
            ddate,
            fmt.unwrap_or_else(|| String::from(ddate::DEFAULT_FMT)),
        )
    } else if !remaining_args.is_empty() {
        return Ok(Usage(Some(format!(
            "wrong number of anonymous arguments: {:?}",
            remaining_args
        ))));
    } else {
        let tm = time::now();
        let ddate = DDate::from_gregorian(tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday)?;
        (
            ddate,
            fmt.unwrap_or_else(|| String::from(ddate::DEFAULT_FMT_LONG)),
        )
    };
    Ok(Done(ddate.format(&fmt)))
}

fn usage(arg0: &str, err: &Option<String>) -> ! {
    let mut stderr = stderr();
    if let Some(ref err) = *err {
        writeln!(stderr, "error: {}\n", err).unwrap();
    }
    writeln!(
        stderr,
        "Display the date in the Discordian calendar.

Usage: {} [+FORMAT] [DD MM YYYY]

Options:
    -h    Display this message
    -v    Print version info and exit

FORMAT controls the output.  Interpreted sequences are:

    %A    Full name of the day of the week (e.g. Sweetmorn)
    %a    Abbreviated name of the day of the week (e.g. SM)
    %B    Full name of the season (e.g. Chaos)
    %b    Abbreviated name of the season (e.g. Chs)
    %d    Ordinal number of day in season (e.g. 23)
    %e    Cardinal number of day in season (e.g. 23rd)
    %H    Name of current Holyday, if any
    %N    Magic code to prevent rest of format from being
          printed unless today is a Holyday.
    %n    Newline
    %t    Tab
    %X    Number of days remaining until X-Day
    %Y    Year (e.g. 3183)
    %.    Try it and see.
    %{{ and %}}   Used to enclose the part of the string
                which is to be replaced with the words
                \"St. Tib's Day\" if the current day is
                St. Tib's Day.",
        arg0
    ).unwrap();
    std::process::exit(if err.is_some() { 1 } else { 0 });
}
